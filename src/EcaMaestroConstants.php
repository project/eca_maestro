<?php

namespace Drupal\eca_maestro;

/**
 * Various constants used in the module code.
 */
class EcaMaestroConstants {

  public const ECA_MAESTRO_LOG_CHANNEL = 'eca_maestro';

  public const ECA_MAESTRO_ID = 'eca_maestro_id';

  public const ECA_MAESTRO_ID_LABEL = 'id';

  public const ECA_MAESTRO_PROCESSID = 'eca_maestro_processid';

  public const ECA_MAESTRO_PROCESSID_LABEL = 'process ID';

  public const ECA_MAESTRO_QUEUEID = 'eca_maestro_queueid';

  public const ECA_MAESTRO_QUEUEID_LABEL = 'queue ID';

  public const ECA_MAESTRO_ASSIGNEE = 'eca_maestro_assignee';

  public const ECA_MAESTRO_ASSIGNEE_LABEL = 'assignee';

  public const ECA_MAESTRO_START = 'eca_maestro_start';

  public const ECA_MAESTRO_START_LABEL = 'start task';

  public const ECA_MAESTRO_STATUS = 'eca_maestro_status';

  public const ECA_MAESTRO_STATUS_LABEL = 'task status';

  public const ECA_MAESTRO_TEMPLATE = 'eca_maestro_template';

  public const ECA_MAESTRO_TEMPLATE_LABEL = 'template name';

  public const ECA_MAESTRO_TOKEN = 'eca_maestro_token';

  public const ECA_MAESTRO_TOKEN_LABEL = 'token name';

  public const ECA_MAESTRO_TYPE = 'eca_maestro_type';

  public const ECA_MAESTRO_TYPE_LABEL = 'type';

  public const ECA_MAESTRO_USERID = 'eca_maestro_userid';

  public const ECA_MAESTRO_USERID_LABEL = 'user ID';

  public const ECA_MAESTRO_VALUE = 'eca_maestro_value';

  public const ECA_MAESTRO_VALUE_LABEL = 'token value';

  public const ECA_MAESTRO_VARNAME = 'eca_maestro_varname';

  public const ECA_MAESTRO_VARNAME_LABEL = 'variable name';

}
