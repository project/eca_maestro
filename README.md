# ECA Maestro

Integrate ECA with the Maestro workflow module.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/eca_maestro).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/eca_maestro).


## Requirements

This module requires the following modules:

- [ECA](https://www.drupal.org/project/eca)
- [Maestro](https://www.drupal.org/project/maestro)


## Installation

Install as you would normally install a contributed Drupal module.
For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

1. Enable the module at Administration > Extend.
2. Add ECA Maestro tasks to your ECA models.

## Maintainers

- Frank Mably - [mably](https://www.drupal.org/u/mably)
