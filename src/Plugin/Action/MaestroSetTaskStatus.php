<?php

namespace Drupal\eca_maestro\Plugin\Action;

use Drupal\Core\Form\FormStateInterface;
use Drupal\eca\Plugin\Action\ConfigurableActionBase;
use Drupal\eca_maestro\EcaMaestroConstants;
use Drupal\eca_maestro\Plugin\MaestroTrait;
use Drupal\maestro\Engine\MaestroEngine;

/**
 * Provide an action to complete a Maestro task.
 *
 * @Action(
 *   id = "eca_maestro_set_task_status",
 *   label = @Translation("Maestro: set task status"),
 *   description = @Translation("Sets the status of a Maestro task corresponding to a specific queue ID.")
 * )
 */
class MaestroSetTaskStatus extends ConfigurableActionBase {

  use MaestroTrait;

  /**
   * {@inheritdoc}
   */
  public function execute(): void {
    $queue_id = $this->getQueueId();
    if (!is_null($queue_id)) {
      $status = $this->getTaskStatus();
      if (!is_null($status)) {
        MaestroEngine::setTaskStatus($queue_id, $status);
        return;
      }
    }
    $this->error($this->t("Maestro set task status failed."));
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      EcaMaestroConstants::ECA_MAESTRO_QUEUEID => '',
      EcaMaestroConstants::ECA_MAESTRO_STATUS => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form[EcaMaestroConstants::ECA_MAESTRO_QUEUEID] = [
      '#type' => 'textfield',
      '#title' => $this->t('Maestro queue ID'),
      '#description' => $this->t('Provide the Maestro queue ID that references the task you want to complete. This property supports tokens.'),
      '#default_value' => $this->configuration[EcaMaestroConstants::ECA_MAESTRO_QUEUEID],
      '#required' => TRUE,
      '#weight' => -20,
    ];
    $form[EcaMaestroConstants::ECA_MAESTRO_STATUS] = [
      '#type' => 'textfield',
      '#title' => $this->t('Maestro task status'),
      '#description' => $this->t('Sets the status of the task (0=active, 1=success, 2=cancel, 3=hold, 4=aborted). This property supports tokens.'),
      '#default_value' => $this->configuration[EcaMaestroConstants::ECA_MAESTRO_STATUS],
      '#required' => TRUE,
      '#weight' => -10,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $this->configuration[EcaMaestroConstants::ECA_MAESTRO_QUEUEID] = $form_state->getValue(EcaMaestroConstants::ECA_MAESTRO_QUEUEID);
    $this->configuration[EcaMaestroConstants::ECA_MAESTRO_STATUS] = $form_state->getValue(EcaMaestroConstants::ECA_MAESTRO_STATUS);
    parent::submitConfigurationForm($form, $form_state);
  }

}
