<?php

namespace Drupal\eca_maestro\Plugin\Action;

use Drupal\Core\Form\FormStateInterface;
use Drupal\eca\Plugin\Action\ConfigurableActionBase;
use Drupal\eca_maestro\EcaMaestroConstants;
use Drupal\eca_maestro\Plugin\MaestroTrait;
use Drupal\maestro\Engine\MaestroEngine;

/**
 * Provide an action to get a Maestro process variable.
 *
 * @Action(
 *   id = "eca_maestro_get_process_variable",
 *   label = @Translation("Maestro: get process variable"),
 *   description = @Translation("Gets the value of a Maestro process variable and puts it in a token.")
 * )
 */
class MaestroGetProcessVariable extends ConfigurableActionBase {

  use MaestroTrait;

  /**
   * {@inheritdoc}
   */
  public function execute(): void {
    $process_id = $this->getProcessId();
    if (!is_null($process_id)) {
      $token_name = $this->getTokenName();
      if (!is_null($token_name)) {
        $variable_name = $this->getVariableName();
        if (!is_null($variable_name)) {
          $value = MaestroEngine::getProcessVariable($variable_name, $process_id);
          $this->getTokenServices()->addTokenData($token_name, $value);
          return;
        }
      }
    }
    $this->error($this->t('Could not get Maestro process variable.'));
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      EcaMaestroConstants::ECA_MAESTRO_TOKEN => '',
      EcaMaestroConstants::ECA_MAESTRO_VARNAME => '',
      EcaMaestroConstants::ECA_MAESTRO_PROCESSID => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form[EcaMaestroConstants::ECA_MAESTRO_TOKEN] = [
      '#type' => 'textfield',
      '#title' => ucfirst($this->t('@label', ['@label' => EcaMaestroConstants::ECA_MAESTRO_TOKEN_LABEL])),
      '#description' => $this->t('The name of the token you want to set the value of the process variable. This property supports tokens.'),
      '#default_value' => $this->configuration[EcaMaestroConstants::ECA_MAESTRO_TOKEN],
      '#required' => TRUE,
      '#weight' => -30,
    ];
    $form[EcaMaestroConstants::ECA_MAESTRO_VARNAME] = [
      '#type' => 'textfield',
      '#title' => $this->t('Maestro @label', ['@label' => EcaMaestroConstants::ECA_MAESTRO_VARNAME_LABEL]),
      '#description' => $this->t('The name of Maestro process variable you want to set the value of the token to. This property supports tokens.'),
      '#default_value' => $this->configuration[EcaMaestroConstants::ECA_MAESTRO_VARNAME],
      '#required' => TRUE,
      '#weight' => -20,
    ];
    $form[EcaMaestroConstants::ECA_MAESTRO_PROCESSID] = [
      '#type' => 'textfield',
      '#title' => $this->t('Maestro @label', ['@label' => EcaMaestroConstants::ECA_MAESTRO_PROCESSID_LABEL]),
      '#description' => $this->t('Provide the process ID you want to set the variable to. This property supports tokens.'),
      '#default_value' => $this->configuration[EcaMaestroConstants::ECA_MAESTRO_PROCESSID],
      '#required' => TRUE,
      '#weight' => -10,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $this->configuration[EcaMaestroConstants::ECA_MAESTRO_TOKEN] = $form_state->getValue(EcaMaestroConstants::ECA_MAESTRO_TOKEN);
    $this->configuration[EcaMaestroConstants::ECA_MAESTRO_VARNAME] = $form_state->getValue(EcaMaestroConstants::ECA_MAESTRO_VARNAME);
    $this->configuration[EcaMaestroConstants::ECA_MAESTRO_PROCESSID] = $form_state->getValue(EcaMaestroConstants::ECA_MAESTRO_PROCESSID);
    parent::submitConfigurationForm($form, $form_state);
  }

}
