<?php

namespace Drupal\eca_maestro\Plugin\Action;

use Drupal\Core\Form\FormStateInterface;
use Drupal\eca\Plugin\Action\ConfigurableActionBase;
use Drupal\eca_maestro\EcaMaestroConstants;
use Drupal\eca_maestro\Plugin\MaestroTrait;
use Drupal\maestro\Engine\MaestroEngine;

/**
 * Provide an action to set a Maestro process variable.
 *
 * @Action(
 *   id = "eca_maestro_set_process_variable",
 *   label = @Translation("Maestro: set process variable"),
 *   description = @Translation("Sets the value of a Maestro process variable.")
 * )
 */
class MaestroSetProcessVariable extends ConfigurableActionBase {

  use MaestroTrait;

  /**
   * {@inheritdoc}
   */
  public function execute(): void {
    $process_id = $this->getProcessId();
    if (!is_null($process_id)) {
      $name = $this->getVariableName();
      if (!is_null($name)) {
        $value = $this->getConfigurationStringValue(
          EcaMaestroConstants::ECA_MAESTRO_VALUE, EcaMaestroConstants::ECA_MAESTRO_VALUE_LABEL);
        if (!is_null($value)) {
          MaestroEngine::setProcessVariable($name, $value, $process_id);
          return;
        }
      }
    }
    $this->error($this->t('Could not set the Maestro process variable.'));
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      EcaMaestroConstants::ECA_MAESTRO_VARNAME => '',
      EcaMaestroConstants::ECA_MAESTRO_VALUE => '',
      EcaMaestroConstants::ECA_MAESTRO_PROCESSID => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form[EcaMaestroConstants::ECA_MAESTRO_VARNAME] = [
      '#type' => 'textfield',
      '#title' => $this->t('Maestro variable name'),
      '#description' => $this->t('The name of the variable you want to set the value of. The process variable must have been added in Maestro template builder beforehand.  This property supports tokens.'),
      '#default_value' => $this->configuration[EcaMaestroConstants::ECA_MAESTRO_VARNAME],
      '#required' => TRUE,
      '#weight' => -30,
    ];
    $form[EcaMaestroConstants::ECA_MAESTRO_VALUE] = [
      '#type' => 'textfield',
      '#title' => $this->t('Maestro variable value'),
      '#description' => $this->t('The value to set the variable to. This property supports tokens.'),
      '#default_value' => $this->configuration[EcaMaestroConstants::ECA_MAESTRO_VALUE],
      '#required' => TRUE,
      '#weight' => -20,
    ];
    $form[EcaMaestroConstants::ECA_MAESTRO_PROCESSID] = [
      '#type' => 'textfield',
      '#title' => $this->t('Maestro process ID'),
      '#description' => $this->t('Provide the process ID you want to set the variable to. This property supports tokens.'),
      '#default_value' => $this->configuration[EcaMaestroConstants::ECA_MAESTRO_PROCESSID],
      '#required' => TRUE,
      '#weight' => -10,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $this->configuration[EcaMaestroConstants::ECA_MAESTRO_VARNAME] = $form_state->getValue(EcaMaestroConstants::ECA_MAESTRO_VARNAME);
    $this->configuration[EcaMaestroConstants::ECA_MAESTRO_VALUE] = $form_state->getValue(EcaMaestroConstants::ECA_MAESTRO_VALUE);
    $this->configuration[EcaMaestroConstants::ECA_MAESTRO_PROCESSID] = $form_state->getValue(EcaMaestroConstants::ECA_MAESTRO_PROCESSID);
    parent::submitConfigurationForm($form, $form_state);
  }

}
