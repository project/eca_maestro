<?php

namespace Drupal\eca_maestro\Plugin\ECA\Condition;

use Drupal\Core\Form\FormStateInterface;
use Drupal\eca\Plugin\ECA\Condition\ConditionBase;
use Drupal\eca_maestro\EcaMaestroConstants;
use Drupal\eca_maestro\Plugin\MaestroTrait;
use Drupal\maestro\Engine\MaestroEngine;

/**
 * ECA condition plugin to check if a user can execute a specific Maestro task.
 *
 * @EcaCondition(
 *   id = "eca_maestro_can_user_execute_task",
 *   label = @Translation("Maestro: can user execute task"),
 *   description = @Translation("Check if provided user can execute the specified task.")
 * )
 */
class MaestroCanUserExecuteTask extends ConditionBase {

  use MaestroTrait;

  /**
   * {@inheritdoc}
   */
  public function evaluate(): bool {
    $queue_id = $this->getQueueId();
    if (!is_null($queue_id)) {
      $user_id = $this->getUserId();
      if (!is_null($user_id)) {
        $can_execute = MaestroEngine::canUserExecuteTask($queue_id, $user_id);
        return $this->negationCheck($can_execute);
      }
    }
    $this->error($this->t("Maestro can user execute task check failed."));
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      EcaMaestroConstants::ECA_MAESTRO_QUEUEID => '',
      EcaMaestroConstants::ECA_MAESTRO_USERID => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form[EcaMaestroConstants::ECA_MAESTRO_QUEUEID] = [
      '#type' => 'textfield',
      '#title' => $this->t('Queue ID'),
      '#description' => $this->t('Provide the Maestro queue ID that references the task you want to complete. This property supports tokens.'),
      '#default_value' => $this->configuration[EcaMaestroConstants::ECA_MAESTRO_QUEUEID],
      '#required' => TRUE,
      '#weight' => -20,
    ];
    $form[EcaMaestroConstants::ECA_MAESTRO_USERID] = [
      '#type' => 'textfield',
      '#title' => $this->t('User ID'),
      '#description' => $this->t('Provide the user ID to check the execution permission from. This property supports tokens.'),
      '#default_value' => $this->configuration[EcaMaestroConstants::ECA_MAESTRO_USERID],
      '#required' => TRUE,
      '#weight' => -10,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $this->configuration[EcaMaestroConstants::ECA_MAESTRO_QUEUEID] = $form_state->getValue(EcaMaestroConstants::ECA_MAESTRO_QUEUEID);
    $this->configuration[EcaMaestroConstants::ECA_MAESTRO_USERID] = $form_state->getValue(EcaMaestroConstants::ECA_MAESTRO_USERID);
    parent::submitConfigurationForm($form, $form_state);
  }

}
