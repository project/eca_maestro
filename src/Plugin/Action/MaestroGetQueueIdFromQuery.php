<?php

namespace Drupal\eca_maestro\Plugin\Action;

use Drupal\eca_endpoint\Plugin\Action\RequestActionBase;
use Drupal\eca_maestro\Plugin\MaestroTrait;
use Drupal\maestro\Engine\MaestroEngine;

/**
 * Get the queue id parameter from the current request query.
 *
 * @Action(
 *   id = "eca_maestro_get_queueid_from_query",
 *   label = @Translation("Maestro: Get the queue Id form the request query")
 * )
 */
class MaestroGetQueueIdFromQuery extends RequestActionBase {

  use MaestroTrait;

  /**
   * {@inheritdoc}
   */
  protected function getRequestValue(): mixed {
    $query = $this->getRequest()->query->all();
    if (isset($query['queueid'])) {
      $queueid = $query['queueid'];
      if (is_numeric($queueid)) {
        return intval($queueid);
      }
    }
    if (isset($query['queueid_or_token'])) {
      $queueid_or_token = $query['queueid_or_token'];
      $queueid = MaestroEngine::getQueueIdFromToken($queueid_or_token);
      if ($queueid) {
        return intval($queueid);
      }
    }
    return NULL;
  }

}
