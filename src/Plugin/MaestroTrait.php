<?php

namespace Drupal\eca_maestro\Plugin;

use Drupal\Core\TypedData\TypedDataInterface;
use Drupal\eca\Token\TokenInterface;
use Drupal\eca_maestro\EcaMaestroConstants;
use Psr\Log\LoggerInterface;

/**
 * Trait for ECA Maestro actions and conditions.
 */
trait MaestroTrait {

  /**
   * The module channel logger object.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected LoggerInterface $customLogger;

  /**
   * Returns the module channel logger object.
   *
   * @return \Psr\Log\LoggerInterface
   *   The module channel logger object.
   */
  protected function getLogger(): LoggerInterface {
    if (!isset($this->customLogger)) {
      $this->customLogger = \Drupal::logger(EcaMaestroConstants::ECA_MAESTRO_LOG_CHANNEL);
    }
    return $this->customLogger;
  }

  /**
   * Returns The ECA-related token services.
   *
   * @return \Drupal\eca\Token\TokenInterface
   *   The token services.
   *
   * @throws \Exception
   */
  protected function getTokenServices(): TokenInterface {
    if (property_exists($this, 'tokenServices')) {
      // ECA 1.x.
      return $this->tokenServices;
    }
    else {
      // ECA 2.x.
      return $this->tokenService;
    }
  }

  /**
   * Log an info message.
   *
   * @param string $message
   *   The log message.
   * @param array $context
   *   The message context.
   */
  public function info(string $message, array $context = []) {
    $this->getLogger()->info($message, $context);
  }

  /**
   * Log an error message.
   *
   * @param string $message
   *   The log message.
   * @param array $context
   *   The message context.
   */
  public function error(string $message, array $context = []) {
    $this->getLogger()->error($message, $context);
  }

  /**
   * Log a warning message.
   *
   * @param string $message
   *   The log message.
   * @param array $context
   *   The message context.
   */
  public function warning(string $message, array $context = []) {
    $this->getLogger()->warning($message, $context);
  }

  /**
   * Build a message for empty configuration.
   *
   * @param string $label
   *   The configuration label.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The resulting message.
   */
  public function emptyMessage(string $label) {
    return $this->t('Empty @label.', ['@label' => $label]);
  }

  /**
   * Build a message for invalid configuration.
   *
   * @param string $label
   *   The configuration label.
   * @param mixed $value
   *   The invalid value.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The resulting message.
   */
  public function invalidMessage(string $label, mixed $value) {
    return $this->t('Invalid @label: @value',
      ['@label' => $label, '@value' => $value]);
  }

  /**
   * Build a message for undefined configuration.
   *
   * @param string $label
   *   The configuration label.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The resulting message.
   */
  public function undefinedMessage(string $label) {
    return $this->t('Undefined @label.', ['@label' => $label]);
  }

  /**
   * Get assigned from configuration.
   *
   * @return string|null
   *   The assignee.
   */
  public function getAssignee($label = EcaMaestroConstants::ECA_MAESTRO_ASSIGNEE): ?string {
    return $this->getConfigurationStringValue(
      EcaMaestroConstants::ECA_MAESTRO_ASSIGNEE, $label, TRUE);
  }

  /**
   * Gets the id from configuration.
   *
   * @return int|null
   *   The id value.
   */
  public function getId($label = EcaMaestroConstants::ECA_MAESTRO_ID_LABEL): ?int {
    return $this->getConfigurationIntValue(
      EcaMaestroConstants::ECA_MAESTRO_ID, $label);
  }

  /**
   * Gets the process id from configuration.
   *
   * @return int|null
   *   The process id.
   */
  public function getProcessId(): ?int {
    return $this->getConfigurationIntValue(
      EcaMaestroConstants::ECA_MAESTRO_PROCESSID, EcaMaestroConstants::ECA_MAESTRO_PROCESSID_LABEL);
  }

  /**
   * Gets the queue id from configuration.
   *
   * @return int|null
   *   The queue id.
   */
  public function getQueueId(): ?int {
    return $this->getConfigurationIntValue(
      EcaMaestroConstants::ECA_MAESTRO_QUEUEID, EcaMaestroConstants::ECA_MAESTRO_QUEUEID_LABEL);
  }

  /**
   * Gets the start task from configuration.
   *
   * @return string|null
   *   The start task.
   */
  public function getStartTask(): ?string {
    return $this->getConfigurationStringValue(
      EcaMaestroConstants::ECA_MAESTRO_START, EcaMaestroConstants::ECA_MAESTRO_START_LABEL);
  }

  /**
   * Gets the task status from configuration.
   *
   * @return int|null
   *   The task status.
   */
  public function getTaskStatus(): ?int {
    return $this->getConfigurationIntValue(
      EcaMaestroConstants::ECA_MAESTRO_STATUS, EcaMaestroConstants::ECA_MAESTRO_STATUS_LABEL);
  }

  /**
   * Gets the template machine name from configuration.
   *
   * @return string|null
   *   The template machine name.
   */
  public function getTemplateMachineName(): ?string {
    return $this->getConfigurationStringValue(
      EcaMaestroConstants::ECA_MAESTRO_TEMPLATE, EcaMaestroConstants::ECA_MAESTRO_TEMPLATE_LABEL, TRUE);
  }

  /**
   * Get the token name from configuration.
   *
   * @param bool $required
   *   The token name configuration is required.
   *
   * @return string|null
   *   The token name.
   */
  public function getTokenName(bool $required = TRUE): ?string {
    return $this->getConfigurationStringValue(
      EcaMaestroConstants::ECA_MAESTRO_TOKEN, EcaMaestroConstants::ECA_MAESTRO_TOKEN_LABEL, $required);
  }

  /**
   * Gets the type from configuration.
   *
   * @return string|null
   *   The type value.
   */
  public function getType($label = EcaMaestroConstants::ECA_MAESTRO_TYPE_LABEL): ?string {
    return $this->getConfigurationStringValue(
      EcaMaestroConstants::ECA_MAESTRO_TYPE, $label, TRUE);
  }

  /**
   * Gets the user id from configuration.
   *
   * @return int|null
   *   The user id.
   */
  public function getUserId(): ?int {
    return $this->getConfigurationIntValue(
      EcaMaestroConstants::ECA_MAESTRO_USERID, EcaMaestroConstants::ECA_MAESTRO_USERID_LABEL);
  }

  /**
   * Gets the variable name from the action plugin configuration.
   *
   * @return string|null
   *   The variable name.
   */
  public function getVariableName(): ?string {
    return $this->getConfigurationStringValue(
      EcaMaestroConstants::ECA_MAESTRO_VARNAME, EcaMaestroConstants::ECA_MAESTRO_VARNAME_LABEL, TRUE);
  }

  /**
   * Gets an integer value from the plugin configuration.
   *
   * @return int|null
   *   The integer value from configuration.
   */
  public function getConfigurationIntValue(
    string $conf_name,
    string $conf_label,
  ): ?int {
    $value = $this->getTokenServices()->getOrReplace(
      $this->configuration[$conf_name]);
    if ($value) {
      if ($value instanceof TypedDataInterface) {
        $value = $value->getValue();
      }
      if (empty($value)) {
        $this->warning($this->emptyMessage($conf_label));
      }
      else {
        if (is_numeric($value)) {
          return intval($value);
        }
        else {
          $this->warning($this->invalidMessage($conf_label, $value));
        }
      }
    }
    else {
      $this->warning($this->undefinedMessage($conf_label));
    }
    return NULL;
  }

  /**
   * Gets a string value from the plugin configuration.
   *
   * @return string|null
   *   The string value from configuration.
   */
  public function getConfigurationStringValue(
    string $conf_name,
    string $conf_label,
    bool $required = FALSE,
  ): ?string {
    $value = $this->getTokenServices()->getOrReplace(
      $this->configuration[$conf_name]);
    if ($value) {
      if ($value instanceof TypedDataInterface) {
        $value = $value->getString();
      }
      if ($required && empty($value)) {
        $this->warning($this->emptyMessage($conf_label));
      }
      else {
        return $value;
      }
    }
    else {
      if ($required) {
        $this->warning($this->undefinedMessage($conf_label));
      }
    }
    return NULL;
  }

  /**
   * Gets a mixed value from the plugin configuration.
   *
   * @return mixed
   *   The mixed value from configuration.
   */
  public function getConfigurationMixedValue(
    string $conf_name,
    string $conf_label,
    bool $required = FALSE,
  ): mixed {
    $value = $this->getTokenServices()->getOrReplace(
      $this->configuration[$conf_name]);
    if ($value) {
      if ($value instanceof TypedDataInterface) {
        $value = $value->getValue();
      }
      if ($required && empty($value)) {
        $this->warning($this->emptyMessage($conf_label));
      }
      else {
        return $value;
      }
    }
    else {
      if ($required) {
        $this->warning($this->undefinedMessage($conf_label));
      }
    }
    return NULL;
  }

}
