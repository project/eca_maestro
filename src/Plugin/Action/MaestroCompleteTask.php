<?php

namespace Drupal\eca_maestro\Plugin\Action;

use Drupal\Core\Form\FormStateInterface;
use Drupal\eca\Plugin\Action\ConfigurableActionBase;
use Drupal\eca_maestro\EcaMaestroConstants;
use Drupal\eca_maestro\Plugin\MaestroTrait;
use Drupal\maestro\Engine\MaestroEngine;

/**
 * Provide an action to complete a Maestro task.
 *
 * @Action(
 *   id = "eca_maestro_complete_task",
 *   label = @Translation("Maestro: complete task"),
 *   description = @Translation("Complete a Maestro task corresponding to a specific queue ID.")
 * )
 */
class MaestroCompleteTask extends ConfigurableActionBase {

  use MaestroTrait;

  /**
   * {@inheritdoc}
   */
  public function execute(): void {
    $queue_id = $this->getQueueId();
    if (!is_null($queue_id)) {
      MaestroEngine::completeTask($queue_id, $this->currentUser->id());
      return;
    }
    $this->error($this->t("Could not set Maestro task as completed."));
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      EcaMaestroConstants::ECA_MAESTRO_QUEUEID => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form[EcaMaestroConstants::ECA_MAESTRO_QUEUEID] = [
      '#type' => 'textfield',
      '#title' => $this->t('Maestro Queue ID'),
      '#description' => $this->t('Provide the Maestro Queue ID that references the task you want to complete. This property supports tokens.'),
      '#default_value' => $this->configuration[EcaMaestroConstants::ECA_MAESTRO_QUEUEID],
      '#required' => TRUE,
      '#weight' => -10,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $this->configuration[EcaMaestroConstants::ECA_MAESTRO_QUEUEID] = $form_state->getValue(EcaMaestroConstants::ECA_MAESTRO_QUEUEID);
    parent::submitConfigurationForm($form, $form_state);
  }

}
