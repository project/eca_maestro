<?php

namespace Drupal\eca_maestro\Plugin\Action;

use Drupal\Core\Form\FormStateInterface;
use Drupal\eca\Plugin\Action\ConfigurableActionBase;
use Drupal\eca_maestro\EcaMaestroConstants;
use Drupal\eca_maestro\Plugin\MaestroTrait;

/**
 * Provide an action to reassign a Maestro task to another user or role.
 *
 * @Action(
 *   id = "eca_maestro_reassign",
 *   label = @Translation("Maestro: reassign task"),
 *   description = @Translation("Reassign a Maestro task to another user or role.")
 * )
 */
class MaestroReassign extends ConfigurableActionBase {

  use MaestroTrait;

  /**
   * {@inheritdoc}
   */
  public function execute(): void {
    $assignment_type = $this->getType();
    if (!is_null($assignment_type)) {
      $assignee = $this->getAssignee();
      if (empty($assignee)) {
        $this->error($this->t('Missing valid assignee'));
      }
      else {
        // Set the field here.
        $assignment_id = $this->getId();
        $assignRecord = $this->entityTypeManager->getStorage('maestro_production_assignments')->load($assignment_id);
        if ($assignRecord) {
          $assignRecord->set('assign_id', $assignee);
          // We force this to be by fixed value now.
          $assignRecord->set('by_variable', '0');
          $assignRecord->save();
          return;
        }
        else {
          $this->error($this->t('Assignment not found: @id',
            ['@id' => $assignment_id]));
        }
      }
    }
    $this->error($this->t("Maestro reassign failed."));
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      EcaMaestroConstants::ECA_MAESTRO_TYPE => 'user',
      EcaMaestroConstants::ECA_MAESTRO_ASSIGNEE => '',
      EcaMaestroConstants::ECA_MAESTRO_ID => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form[EcaMaestroConstants::ECA_MAESTRO_TYPE] = [
      '#type' => 'textfield',
      '#title' => $this->t('Assignee type'),
      '#description' => $this->t('Provide the type of the new assignee (user or role).'),
      '#default_value' => $this->configuration[EcaMaestroConstants::ECA_MAESTRO_TYPE],
      '#required' => TRUE,
      '#weight' => -30,
    ];
    $form[EcaMaestroConstants::ECA_MAESTRO_ASSIGNEE] = [
      '#type' => 'textfield',
      '#title' => $this->t('Task assignee'),
      '#description' => $this->t('User display name or role name of the new assignee.'),
      '#default_value' => $this->configuration[EcaMaestroConstants::ECA_MAESTRO_ASSIGNEE],
      '#required' => TRUE,
      '#weight' => -20,
    ];
    $form[EcaMaestroConstants::ECA_MAESTRO_ID] = [
      '#type' => 'number',
      '#title' => $this->t('Assignment ID'),
      '#default_value' => $this->configuration[EcaMaestroConstants::ECA_MAESTRO_ID],
      '#min' => 1,
      '#required' => TRUE,
      '#weight' => -10,
      '#description' => $this->t('Provide the id of the assignment to reassign.'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $this->configuration[EcaMaestroConstants::ECA_MAESTRO_TYPE] = $form_state->getValue(EcaMaestroConstants::ECA_MAESTRO_TYPE);
    $this->configuration[EcaMaestroConstants::ECA_MAESTRO_ASSIGNEE] = $form_state->getValue(EcaMaestroConstants::ECA_MAESTRO_ASSIGNEE);
    $this->configuration[EcaMaestroConstants::ECA_MAESTRO_ID] = $form_state->getValue(EcaMaestroConstants::ECA_MAESTRO_ID);
    parent::submitConfigurationForm($form, $form_state);
  }

}
