<?php

namespace Drupal\eca_maestro\Plugin\Action;

use Drupal\Core\Form\FormStateInterface;
use Drupal\eca\Plugin\Action\ConfigurableActionBase;
use Drupal\eca_maestro\EcaMaestroConstants;
use Drupal\eca_maestro\Plugin\MaestroTrait;
use Drupal\maestro\Engine\MaestroEngine;

/**
 * Provide an action to launch a new Maestro process.
 *
 * @Action(
 *   id = "eca_maestro_new_process",
 *   label = @Translation("Maestro: new process"),
 *   description = @Translation("Launch a new Maestro process corresponding to the template and start task provided.")
 * )
 */
class MaestroNewProcess extends ConfigurableActionBase {

  use MaestroTrait;

  /**
   * {@inheritdoc}
   */
  public function execute(): void {
    $template_name = $this->getTemplateMachineName();
    if (!is_null($template_name)) {
      $template = MaestroEngine::getTemplate($template_name);
      if ($template) {
        $engine = new MaestroEngine();
        $start_task = $this->getStartTask();
        $pid = $engine->newProcess($template_name, $start_task ?: 'start');
        if ($pid) {
          $token_name = $this->getTokenName(FALSE);
          if (!is_null($token_name)) {
            $this->getTokenServices()->addTokenData($token_name, $pid);
          }
          $this->info($this->t('Process started (@pid)', ['@pid' => $pid]));
          // @todo needed?
          // Run the orchestrator for us once on process kickoff.
          /*$orchestrator = new MaestroOrchestrator();
          $config = \Drupal::config('maestro.settings');
          $orchestrator->orchestrate(
          $config->get('maestro_orchestrator_token'), TRUE);*/
          return;
        }
      }
      else {
        $this->warning($this->t('Invalid template name: @name',
          ['@name' => $template_name]));
      }
    }
    $this->error($this->t("Maestro new process failed."));
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      EcaMaestroConstants::ECA_MAESTRO_TEMPLATE => '',
      EcaMaestroConstants::ECA_MAESTRO_START => '',
      EcaMaestroConstants::ECA_MAESTRO_TOKEN => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form[EcaMaestroConstants::ECA_MAESTRO_TEMPLATE] = [
      '#type' => 'textfield',
      '#title' => $this->t('Maestro template name'),
      '#description' => $this->t('Provide the Maestro template name of the process we want to instantiate. This property supports tokens.'),
      '#default_value' => $this->configuration[EcaMaestroConstants::ECA_MAESTRO_TEMPLATE],
      '#required' => TRUE,
      '#weight' => -30,
    ];
    $form[EcaMaestroConstants::ECA_MAESTRO_START] = [
      '#type' => 'textfield',
      '#title' => $this->t('Maestro start task task'),
      '#description' => $this->t('Sets the status of the task (default: start). This property supports tokens.'),
      '#default_value' => $this->configuration[EcaMaestroConstants::ECA_MAESTRO_START],
      '#required' => FALSE,
      '#weight' => -20,
    ];
    $form[EcaMaestroConstants::ECA_MAESTRO_TOKEN] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name of the process ID token'),
      '#default_value' => $this->configuration[EcaMaestroConstants::ECA_MAESTRO_TOKEN],
      '#required' => FALSE,
      '#weight' => -10,
      '#description' => $this->t('Provide the name of a token where the value of the new Maestro process ID should be stored.'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $this->configuration[EcaMaestroConstants::ECA_MAESTRO_TEMPLATE] = $form_state->getValue(EcaMaestroConstants::ECA_MAESTRO_TEMPLATE);
    $this->configuration[EcaMaestroConstants::ECA_MAESTRO_START] = $form_state->getValue(EcaMaestroConstants::ECA_MAESTRO_START);
    $this->configuration[EcaMaestroConstants::ECA_MAESTRO_TOKEN] = $form_state->getValue(EcaMaestroConstants::ECA_MAESTRO_TOKEN);
    parent::submitConfigurationForm($form, $form_state);
  }

}
