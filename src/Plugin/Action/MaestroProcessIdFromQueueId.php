<?php

namespace Drupal\eca_maestro\Plugin\Action;

use Drupal\Core\Form\FormStateInterface;
use Drupal\eca\Plugin\Action\ConfigurableActionBase;
use Drupal\eca_maestro\EcaMaestroConstants;
use Drupal\eca_maestro\Plugin\MaestroTrait;
use Drupal\maestro\Engine\MaestroEngine;

/**
 * Action to set a token with the process ID from the queue ID.
 *
 * @Action(
 *   id = "eca_maestro_process_id_from_queue_id",
 *   label = @Translation("Maestro: set process id from queue id"),
 *   description = @Translation("Define a locally available token by a specific name and a value containing the process id relating to the given queue id.")
 * )
 */
class MaestroProcessIdFromQueueId extends ConfigurableActionBase {

  use MaestroTrait;

  /**
   * {@inheritdoc}
   */
  public function execute(): void {
    $queue_id = $this->getQueueId();
    if (!is_null($queue_id)) {
      $token_name = $this->getTokenName();
      if (!is_null($token_name)) {
        $process_id = MaestroEngine::getProcessIdFromQueueId($queue_id);
        $this->getTokenServices()->addTokenData($token_name, $process_id);
        return;
      }
    }
    $this->error($this->t('Could not get the process ID from the queue ID.'));
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      EcaMaestroConstants::ECA_MAESTRO_TOKEN => '',
      EcaMaestroConstants::ECA_MAESTRO_QUEUEID => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form[EcaMaestroConstants::ECA_MAESTRO_TOKEN] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name of the process ID token'),
      '#default_value' => $this->configuration[EcaMaestroConstants::ECA_MAESTRO_TOKEN],
      '#required' => TRUE,
      '#weight' => -20,
      '#description' => $this->t('Provide the name of a token where the value of the Maestro process ID should be stored.'),
    ];
    $form[EcaMaestroConstants::ECA_MAESTRO_QUEUEID] = [
      '#type' => 'textarea',
      '#title' => $this->t('Queue ID value'),
      '#default_value' => $this->configuration[EcaMaestroConstants::ECA_MAESTRO_QUEUEID],
      '#required' => TRUE,
      '#weight' => -10,
      '#description' => $this->t('The value of the Maestro queue ID we want to get the process ID from.'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $this->configuration[EcaMaestroConstants::ECA_MAESTRO_TOKEN] = $form_state->getValue(EcaMaestroConstants::ECA_MAESTRO_TOKEN);
    $this->configuration[EcaMaestroConstants::ECA_MAESTRO_QUEUEID] = $form_state->getValue(EcaMaestroConstants::ECA_MAESTRO_QUEUEID);
    parent::submitConfigurationForm($form, $form_state);
  }

}
